from django.forms import ModelChoiceField
from django.shortcuts import render, redirect
from django.urls import Resolver404, reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm


# Create your views here.
class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


# class ReceiptCreateView(LoginRequiredMixin, CreateView):
#     model = Receipt
#     template_name = "receipts/new_receipt.html"
#     fields = ["vendor", "total", "tax", "date", "category", "account"]
#     success_url = reverse_lazy("home")

#     def form_valid(self, form):
#         form.instance.purchaser = self.request.user
#         return super().form_valid(form)


def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.instance
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
        # form.category = ModelChoiceField(
        #     queryset=Receipt.category.values_list(owner=request.user)
        # )
    context = {"form": form}
    return render(request, "receipts/new_receipt.html", context)


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "receipts/categories.html"
    # context_object_name = "expensecategory_list"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/accounts.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipts/new_category.html"
    fields = ["name"]
    success_url = reverse_lazy("categories")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receipts/new_account.html"
    fields = ["name", "number"]
    success_url = reverse_lazy("myaccounts")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)
